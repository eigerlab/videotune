package com.yourringtone;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.ads.InterstitialAd;

public class PlayVideoLayoutChooseForRingtone {
    private InterstitialAd mInterstitialAd;


    public static void show(final Context context, final String url){
//        Adds addsClass=new Adds(context);
//        addsClass.showInterstitial();



        final Dialog dialog = new Dialog(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.recyclerlayoutdialog);
        final VideoView videoView=dialog.findViewById(R.id.videoplay);
        Button btn =dialog.findViewById(R.id.setringtone);
        videoView.setVideoPath(url);
        videoView.start();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog settingsDialog = new Dialog(context);
                LayoutInflater layoutInflater1 = LayoutInflater.from(view.getContext());
                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

                settingsDialog.setContentView(layoutInflater1.inflate(R.layout.contactdialog, null));
                settingsDialog.show();

                final TextView allcontact=settingsDialog.findViewById(R.id.allcontact);
                allcontact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedPreferences sharedPreferences = context.getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("all_contact",url);
                        Log.i("ghh",url);
                        editor.commit();
                        editor.apply();
                        settingsDialog.dismiss();
                        dialog.dismiss();
                       // content://com.miui.gallery.open/raw/%2Fstorage%2Femulated%2F0%2FDCIM%2FCamera%2FVID_20201024_162749.mp4...gallery
                        //content://com.android.providers.media.documents/document/video%3A502127....storage
                        Toast.makeText(context, "Saved.", Toast.LENGTH_SHORT).show();
                       // content://com.mi.android.globalFileexplorer.myprovider/external_files/video_2017_Aug_27_20_13_59.mp4
                    }
                });
                LinearLayout contimg=settingsDialog.findViewById(R.id.lener);
                contimg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        settingsDialog.dismiss();
                        dialog.dismiss();
                        Intent intent=new Intent(context,ContactActivity.class);
                        intent.putExtra("videoUrl",url);
                        context.startActivity(intent);
                    }
                });
            }
        });
        dialog.show();
    }
}
