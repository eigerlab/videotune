package com.yourringtone;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.yourringtone.service.PhoneTrackService;

public class CallRecevicer extends BroadcastReceiver {
    private static boolean incomingFlag = false;
    private static String incoming_number = null;

    @Override
    public void onReceive(final Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            incomingFlag = false;
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//            Toast.makeText(context, "New Outgoing Call " + phoneNumber, Toast.LENGTH_LONG).show();
        } else {
            Intent intent1 = null;
//            Toast.makeText(context, "Not New Outgoing Call ", Toast.LENGTH_LONG).show();


            try {
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//                    adjustAudio(false, context);
                    Boolean isShow = context.getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE).getBoolean("isVideoRingtone", true);
                    if (isShow) {
                        incomingFlag = true;
                        incoming_number = intent.getStringExtra("incoming_number");
                        if (incomingFlag && (incoming_number != null)) {
                            intent = new Intent(context, PhoneTrackService.class);
                            intent.putExtra("number", incoming_number);
                            intent.putExtra("name", getContactName(context, incoming_number));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                context.startForegroundService(intent);

                            } else {
                                context.startService(intent);

                            }
                        }
                    }

                }
                if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))) {
                    Toast.makeText(context, "Call Received State", Toast.LENGTH_SHORT).show();
                }
                if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            context.stopService(new Intent(context, PhoneTrackService.class));
                        } else {
                            context.stopService(new Intent(context, PhoneTrackService.class));
                        }
                        if (intent1 != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                context.stopService(intent1);
                            } else {
                                context.stopService(intent1);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(context, "Call Idle State", Toast.LENGTH_SHORT).show();
                    context.stopService(new Intent(context, PhoneTrackService.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private String getContactName(Context context, String number) {
        Log.i("FAFAFAF", number + "fafa");
        String returnName = "";

        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        if (number != null) {
            while (cursor.moveToNext()) {
                String num = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                String num2 = num.replace("+91", "").replace("-", "").replace(" ", "");
                String number2 = number.replace("+91", "").replace("-", "").replace(" ", "");

                String num2ChatAt = num2.charAt(0) + "";
                if (num2ChatAt.equals("0")) {
                    num2 = num2.substring(1, num2.length() - 1);
                }

                String number2ChatAt = number2.charAt(0) + "";
                if (number2ChatAt.equals("0")) {
                    number2 = number2.substring(1, number2.length() - 1);
                }

                if (num2.equals(number2)) {
                    returnName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    Log.i("hbjhj", returnName);
                    return returnName;
                }
            }
        }

        return returnName;
    }


    public void adjustAudio(boolean setMute, Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int adJustMute = 0;
            if (setMute) {
                adJustMute = AudioManager.ADJUST_MUTE;

            } else {
                adJustMute = AudioManager.ADJUST_UNMUTE;
            }
            audioManager.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, adJustMute, 0);
            audioManager.adjustStreamVolume(AudioManager.STREAM_ALARM, adJustMute, 0);
            audioManager.adjustStreamVolume(AudioManager.RINGER_MODE_VIBRATE, adJustMute, 0);
            // audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, adJustMute, 0);
            audioManager.adjustStreamVolume(AudioManager.STREAM_RING, adJustMute, 0);
            audioManager.adjustStreamVolume(AudioManager.STREAM_SYSTEM, adJustMute, 0);
        } else {
            audioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, setMute);
            audioManager.setStreamMute(AudioManager.STREAM_ALARM, setMute);
            audioManager.setStreamMute(AudioManager.RINGER_MODE_VIBRATE, setMute);
            // audioManager.setStreamMute(AudioManager.STREAM_MUSIC, setMute);
            audioManager.setStreamMute(AudioManager.STREAM_RING, setMute);
            audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, setMute);
        }
    }

}










